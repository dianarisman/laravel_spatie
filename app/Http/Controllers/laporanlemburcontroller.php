<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\laporanlembur;
use Auth;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;

class laporanlemburcontroller extends Controller
{
    //
      //
      public function __construct()
      {
          $this->middleware('auth');
      }
  
      public function index() {
          date_default_timezone_set('asia/ho_chi_minh');
          $format = 'Y/m/d';
          $now = date($format);
          $to = date($format, strtotime("+30 days"));
          $constraints = [
              'from' => $now,
              'to' => $to
          ];
  
          $lembur = $this->getHiredEmployees($constraints);
          return view('laporanlembur/index', ['lembur' => $lembur, 'searchingVals' => $constraints]);
      }
  
      private function getHiredEmployees($constraints) {
          $absensi = laporanlembur::where('tanggal', '>=', $constraints['from'])
                          ->where('tanggal', '<=', $constraints['to'])
                          ->get();
          return $absensi;
      }
  
      public function search(Request $request) {
          $constraints = [
              'from' => $request['from'],
              'to' => $request['to']
          ];
  
          $lembur = $this->getHiredEmployees($constraints);
          return view('laporanlembur/index', ['lembur' => $lembur, 'searchingVals' => $constraints]);
      }
}
