<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lokasi;

class lokasicontroller extends Controller
{
    //
    public function index()
    {
        $lokasi = lokasi::all();
        return view('lokasi.index', compact('lokasi'));
    }

    public function create()
    {
        return view('lokasi.create');
    }

  
    public function store(Request $request)
    {
        $validasi = $request->validate([
            'nama' => 'required',
            'lat'=> 'required',
            'long' => 'required',
        ]);
        $lokasi = lokasi::create($validasi);
   
        return redirect('/lokasi')->with('success', 'Selamat data berhasil ditambah!');
    }

    public function edit(lokasi $lokasi)
    {
        return view('lokasi.edit', compact('lokasi'));
    }

    public function update(Request $request, $id)
    {
        $validasi = $request->validate([
            'nama' => 'required',
            'lat'=> 'required',
            'long' => 'required',
        ]);
        lokasi::whereId($id)->update($validasi);

        return redirect('/lokasi')->with('success', 'Data berhasil di update');
    }

    public function destroy($id)
    {
        $lokasi = lokasi::findOrFail($id);
        $lokasi->delete();

        return redirect('/lokasi')->with('success', 'Data berhasil dihapus!');
    }
}
