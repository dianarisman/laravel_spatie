<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\laporan;
class AbsenController extends Controller
{
    //
  //UNTUK MOBILE
  public function absenmasuk(Request $request){
    try {
        DB::beginTransaction();
  
        // get the current time  4
        $current = Carbon::now();
        $current->timezone = 'Asia/Jakarta';
        $jamMasuk = $current->toTimeString();
        $tanggalMasuk= $current->toDateString();
  
        $kar = DB::table('karyawan')->where('ID',$request->ID_Karyawan)->first();
  
        if (DB::table('absensi')->where('ID_Karyawan',$request->ID_Karyawan)->where('Tanggal',$tanggalMasuk)->first() != null) {
              $response = [
                'statusCode'    => 500,
                'message'   => 'Anda Sudah Absen',
            ];
        return response()->json(['result'=>$response],500);
        }

        $lok = DB::table('lokasi')->get();
       
        $lat1 = $request->lat;
        $lon1 = $request->long;
  
        $data_dist = [];
        for ($i=0; $i < count($lok) ; $i++) { 
            # code...
            $lat2 = $lok[$i]->lat;
            $lon2 = $lok[$i]->long;
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
      
            $jarak = ($miles * 1.609344) * 1000;
            array_push($data_dist,$jarak);
            
        }
       if (min($data_dist) > 50) {
           $response = [
                'statusCode'    => 202,
                'message'   => 'Anda Diluar Range Kantor',
            ];

        return response()->json(['result'=>$response],200);
       }
       
        $status ='';
        if($jamMasuk<='07:00'){
        $status='Tepat Waktu';
        }else{
        $status='Terlambat';
        }
        DB::table('absensi')->insert(['ID_Karyawan'=>$request->ID_Karyawan,'Tanggal'=>$tanggalMasuk,'jam_masuk'=>$jamMasuk,'latitude'=>$lat1,'longitude'=>$lon1,'Keterangan'=>$status]);
        DB::commit();
         $response = [
                'statusCode'    => 200,
                'message'   => 'Absen Berhasil',
            ];
        return response()->json(['result'=>$response],200);
      } catch (Exception $e) {
        DB::rollback();
        return response()->json(['messages'=>$e],200);
      }
}
public function absenkeluar (Request $request){
    // get the current time  4
    $current = Carbon::now();
    $current->timezone = 'Asia/Jakarta';
    $jamKeluar = $current->toTimeString();
    $tanggalKeluar = $current->toDateString();
  
    $kar = DB::table('karyawan')->where('ID',$request->ID_Karyawan)->first();
  
    if (DB::table('absensi')->where('ID_Karyawan',$request->ID_Karyawan)->where('Tanggal',$tanggalKeluar)->where('jam_keluar','!=',null)->first() != null) {
          $response = [
                  'statusCode'    => 500,
                  'message'   => 'Anda Sudah Absen',
              ];
      return response()->json(['result'=>$response],500);
    }

    $lok = DB::table('lokasi')->get();
  
    $lat1 = $request->lat;
    $lon1 = $request->long;
  
    $data_dist = [];
          for ($i=0; $i < count($lok) ; $i++) { 
              # code...
              $lat2 = $lok[$i]->lat;
              $lon2 = $lok[$i]->long;
              $theta = $lon1 - $lon2;
              $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
              $dist = acos($dist);
              $dist = rad2deg($dist);
              $miles = $dist * 60 * 1.1515;
        
              $jarak = ($miles * 1.609344) * 1000;
              array_push($data_dist,$jarak);
              
          }
  
          if (min($data_dist) > 50) {
            $response = [
                  'statusCode'    => 202,
                  'message'   => 'Anda Diluar Range Kantor',
              ];

          return response()->json(['result'=>$response],202);
           }
           $status='';
           if($jamKeluar<='17:00'){
            DB::table('absensi')
              ->where('ID_Karyawan', $request->ID_Karyawan)
              ->where('Tanggal', $tanggalKeluar)
              ->update(['jam_keluar' => $jamKeluar]);
            }else{
            $status='Lembur';
            DB::table('absensi')
              ->where('ID_Karyawan', $request->ID_Karyawan)
              ->where('Tanggal', $tanggalKeluar)
              ->update(['jam_keluar' => '17:00:00']);
            DB::table('lembur')->insert(['ID_Karyawan'=>$request->ID_Karyawan,'Tanggal'=>$tanggalKeluar,'jam_mulai'=>'17:00:00','jam_selesai'=>$jamKeluar,'latitude'=>$lat1,'longitude'=>$lon1,'Keterangan'=>$status]);
            DB::commit();
            }
      $response = [
                  'statusCode'    => 200,
                  'message'   => 'Absen Keluar Berhasil',
              ];
          return response()->json(['result'=>$response],200);
  }

  public function absenFilter($awal, $akhir){

    $hasil = DB::table('absensi')->join('karyawan','absensi.ID_Karyawan','=','karyawan.ID')
              ->whereDate('absensi.Tanggal', '>=', date($awal))
              ->whereDate('absensi.Tanggal', '<=', date($akhir))
              ->get();
      return  view ('absensi')->with(['data_absen'=>$hasil,'start'=>$awal,'end'=>$akhir]);

  }

  public function getabsen($id){
       // Find the user by email
      $data = DB::table('absensi')
      ->leftJoin('karyawan','karyawan.id','=','absensi.id_karyawan')
      ->leftJoin('jabatan','karyawan.id_jabatan','=','jabatan.id')
      ->select('karyawan.id','karyawan.nama','karyawan.nik','karyawan.telp','jabatan.nama as jabatan','absensi.tanggal', 'absensi.jam_masuk as jam_masuk','absensi.jam_keluar as jam_keluar','absensi.keterangan as keterangan')
      ->where('karyawan.id','=',$id)
      ->get();
       $response = [
                  'statusCode'    => 200,
                  'message'   => 'Berhasil Menampilkan Data',
                  'Data' =>$data
              ];
    return response()->json(['result'=>$response],200);
  }
  
   public function getlembur($id){
       // Find the user by email
      $data = DB::table('lembur')
      ->leftJoin('lembur','karyawan.id','=','lembur.id_karyawan')
      ->leftJoin('jabatan','karyawan.id_jabatan','=','jabatan.id')
      ->select('karyawan.id','karyawan.nama','karyawan.nik','karyawan.telp','jabatan.nama as jabatan','lembur.tanggal', 'lembur.jam_mulai as jam_mulai','lembur.jam_selesai as jam_selesai','lembur.keterangan as keterangan')
      ->where('karyawan.id','=',$id)
      ->get();
       $response = [
                  'statusCode'    => 200,
                  'message'   => 'Berhasil Menampilkan Data',
                  'Data' =>$data
              ];
    return response()->json(['result'=>$response],200);
  }
}
