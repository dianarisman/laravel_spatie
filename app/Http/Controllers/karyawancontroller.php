<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\jabatan;
use App\User;
use App\karyawan;
class karyawancontroller extends Controller
{
    //
    public function index()
    {
        $dk = DB::table('karyawan')
        ->leftJoin('jabatan','karyawan.id_jabatan','=','jabatan.id')
        ->select('karyawan.*','jabatan.nama as nama_jabatan')
        ->paginate(10);
        return view('karyawan.index', compact('dk'));
    }
    public function create()
    {
        $jabatan = jabatan::orderBy('nama', 'ASC')->get();
        return view('karyawan.create',compact('jabatan'));
    }

    public function store(Request $request)
    {
        if ($request->nik == '' || $request->nama == '' || $request->password == ''){
            session()->flash('message','Tidak Boleh ada kolom kosong');
            return view('karyawan.index');
          }
      
           if (DB::table('karyawan')->where('NIK',$request->nik)->first() != null) {
           session()->flash('message','Anda Sudah Terdaftar');
           return view('dashboard.index');
           }else{
      
            DB::table('users')->insert([
              'name'=>$request->nama,
              'email'=>$request->email,
              'password'=>Hash::make($request->password)
            ]);
         
            DB::table('karyawan')->insert([
              'id_user' => User::all()->last()->id,
              'nama'=>$request->nama,
              'nik'=>$request->nik,
              'telp'=>$request->telp,
              'id_jabatan'=>$request->id_jabatan
            ]);
            
            return redirect('/karyawan')->with('success', 'Selamat data berhasil ditambah!');
          }
    }

    public function edit($id)
    {
       $karyawan = karyawan::findOrFail($id);
       $user = User::findOrFail($karyawan->id_user);
       $jabatan = jabatan::orderBy('nama', 'ASC')->get();
      return view('karyawan.edit', compact('karyawan', 'jabatan','user'));

    }

    public function update(Request $request, $id)
    {
       //validasi
    $this->validate($request, [
    
      'nama' => 'required|string|max:100',
      'nik' => 'required|integer',
      'email' => 'required',
      'telp' => 'required',
      'id_jabatan' => 'required',
    ]);
    $karyawan = karyawan::findOrFail($id);
    

    //perbaharui data di database
    $karyawan->update([
        'nama' => $request->nama,
        'nik' => $request->nik,
        'id_jabatan' => $request->id_jabatan,
    ]);

    $user = User::findOrFail($karyawan->id_user);

    $user->update([
      'email' => $request->email,
      'password' => $request->password,
      'nama' => $request->nama,
    ]);
    return redirect('/karyawan')->with('success', 'Data berhasil di update');
    }


    public function destroy($id)
    {
        $karyawan = Karyawan::findOrFail($id);
        $user = User::findOrFail($karyawan->id_user);
        $karyawan->delete();
        $user->delete();

        return redirect('/karyawan')->with('success', 'Data berhasil dihapus!');
    }
}
