<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\absen;
use Auth;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;

class laporanabsensicontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        date_default_timezone_set('asia/ho_chi_minh');
        $format = 'Y/m/d';
        $now = date($format);
        $to = date($format, strtotime("+30 days"));
        $constraints = [
            'from' => $now,
            'to' => $to
        ];

        $absensi = $this->getHiredEmployees($constraints);
        return view('laporanabsensi/index', ['absensi' => $absensi, 'searchingVals' => $constraints]);
    }

    private function getHiredEmployees($constraints) {
        $absensi = absen::where('tanggal', '>=', $constraints['from'])
                        ->where('tanggal', '<=', $constraints['to'])
                        ->get();
        return $absensi;
    }

    public function search(Request $request) {
        $constraints = [
            'from' => $request['from'],
            'to' => $request['to']
        ];

        $absensi = $this->getHiredEmployees($constraints);
        return view('laporanabsensi/index', ['absensi' => $absensi, 'searchingVals' => $constraints]);
    }

    public function exportExcel(Request $request) {
        $this->prepareExportingData($request)->export('xlsx');
        redirect()->intended('laporanabsensi');
    }

    private function prepareExportingData($request) {
        $author = Auth::user()->name;
        $employees = $this->getExportingData(['from'=> $request['from'], 'to' => $request['to']]);
    
        return Excel::download(new $employees, 'users.xlsx');
    }

    private function getExportingData($constraints) {
        return DB::table('laporan_absensi')
        ->where('tanggal', '>=', $constraints['from'])
        ->where('tanggal', '<=', $constraints['to'])
        ->get()
        ->map(function ($item, $key) {
        return (array) $item;
        })
        ->all();
    }



}
