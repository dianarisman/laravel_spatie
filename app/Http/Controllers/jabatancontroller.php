<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jabatan;

class jabatancontroller extends Controller
{
    //
    public function index()
    {
        //
        $jabatan = Jabatan::all();
        return view('jabatan.index', compact('jabatan'));
    }
    public function show(Jabatan $jabatan)
    {
        return $jabatan;
    }

    public function create()
    {
        return view('jabatan.create');
    }

    public function store(Request $request)
    {
        $validasi = $request->validate([
            'nama' => 'required',
           
        ]);
        $jabatan = Jabatan::create($validasi);
   
        return redirect('/jabatan')->with('success', 'Selamat data berhasil ditambah!');
    }

    public function edit(Jabatan $jabatan)
    {
        return view('jabatan.edit', compact('jabatan'));
    }

    public function update(Request $request, $id)
    {
        $validasi = $request->validate([
            'nama' => 'required'
        ]);
        Jabatan::whereId($id)->update($validasi);

        return redirect('/jabatan')->with('success', 'Data berhasil di update');
    }

    public function destroy( $id)
    {
        $jabatan = Jabatan::findOrFail($id);
        $jabatan->delete();

        return redirect('/jabatan')->with('success', 'Data berhasil dihapus!');
    }
}
