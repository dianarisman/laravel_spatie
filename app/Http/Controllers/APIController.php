<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;

class APIController extends Controller

{
    public $loginAfterSignUp = true;
    //
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

          // Find the user by email
          $user = DB::table('karyawan')
          ->leftJoin('jabatan','karyawan.id_jabatan','=','jabatan.id')
          ->leftJoin('users','karyawan.id_user','=','users.id')
          ->select('karyawan.id','karyawan.nama','karyawan.nik','karyawan.telp','jabatan.nama as jabatan')
          ->where('users.email','=',$request->get('email'))
          ->first();

          $response = [
            'StatusCode'    => 200,
            'message'   => 'Login Berhasil',
            'token'      => $token,
            'Data' => $user,
        ];   

        return response()->json(['result' =>$response ]);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    /**
     * @param RegistrationFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

   
 
    public function register(RegisterAuthRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
 
        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }
 
        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }
    
}
