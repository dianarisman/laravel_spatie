<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class laporanlembur extends Model
{
    //
    protected $table = 'laporan_lembur';

    /**
     * @var array
     */
    protected $guarded = [];
}
