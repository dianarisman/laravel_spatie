<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function () {

	return view('auth.login');
});

Route::group(['middleware' => 'admin'], function() {

	Route::get('/home', function () {
		$data = App\karyawan::count();
		$jml_absensi = App\absen::count();
		$jml_lembur = App\laporanlembur::count();
		$jml_lokasi = App\lokasi::count();
		return view('home', compact('data','jml_absensi','jml_lembur','jml_lokasi'));
	});

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/routes', 'HomeController@admin');
Route::resource('karyawan','KaryawanController');
Route::resource('jabatan','jabatancontroller');
Route::resource('lokasi','lokasicontroller');

Route::get('laporanabsensi', 'laporanabsensicontroller@index');
Route::post('laporanabsensi/search', 'laporanabsensicontroller@search')->name('report.search');
Route::get('laporanlembur', 'laporanlemburcontroller@index');
Route::post('laporanlembur/search', 'laporanlemburcontroller@search')->name('lamporanlembur.search');
});