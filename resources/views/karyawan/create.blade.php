@extends ('layouts.main')

@section('layout')
<div class="container-fluid">
<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tambah Karyawan</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('karyawan.store') }}">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" />
            </div>
            <div class="form-group">
                <label>Nik</label>
                <input type="text" class="form-control" name="nik" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email" />
            </div>
            <div class="form-group">
                <label>Nomor Telepon</label>
                <input type="text" class="form-control" name="telp" />
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" />
            </div>
            <div class="form-group">
                <label for="">Jabatan</label>
                <select name="id_jabatan" id="id_jabatan" required
                    class="form-control ">
                    <option value="">Pilih</option>
                    @foreach ($jabatan as $row)
                    <option value="{{ $row->id }}">{{ ucfirst($row->nama) }}</option>
                    @endforeach
                </select>
                <p class="text-danger">{{ $errors->first('id_jabatan') }}</p>
            </div>
            <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
    </div>
</div>
@endsection