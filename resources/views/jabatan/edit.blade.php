@extends ('layouts.main')
@section('layout')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Ubah Jabatan</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif

        <form action="{{ route('jabatan.update',$jabatan->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" value="{{ $jabatan->nama }}" />
            </div>
            <button type="submit" class="btn btn-primary">Edit Data</button>
    </div>

    </form>
</div>
</div>
@endsection
