<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Presensi</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('/assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('/assets/css/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{asset('/assets/css/timeline.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('/assets/css/startmin.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset('/assets/css/morris.css')}}" rel="stylesheet">
    <link href="{{ asset('/assets/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css" />

    <!-- Custom Fonts -->
    <link href="{{asset('/assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <!-- <link href="{{asset('/assets/css/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet"> -->

    <!-- DataTables Responsive CSS -->
    <!-- <link href="{{asset('/assets/css/dataTables/dataTables.responsive.css')}}" rel="stylesheet"> -->

    <!-- Custom CSS -->
    <link href="{{asset('/assets/css/startmin.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <!-- <link href="{{asset('/assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

    

</head>

<body>

    <div id="wrapper">

        @include('layouts.header')

        <div id="page-wrapper">
            @yield('layout')
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->

    <script src="{{asset('/assets/js/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('/assets/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('/assets/js/metisMenu.min.js')}}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{asset('/assets/js/raphael.min.js')}}"></script>
    <script src="{{asset('/assets/js/morris.min.js')}}"></script>
    <script src="{{asset('/assets/js/morris-data.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{asset('/assets/js/startmin.js')}}"></script>

    <!-- DataTables JavaScript -->
    <!-- <script src="{{asset('/assets/js/dataTables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/assets/js/dataTables/dataTables.bootstrap.min.js')}}"></script> -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->


    <!-- Custom Theme JavaScript -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script src="{{ asset ('/assets/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('#table-datatables').DataTable({
                dom: 'Bfrtip',
                buttons: ['excel', 'print']
            });
        });

    </script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            //Date picker
            $('#date').datepicker({
                autoclose: true,
                format: 'yyyy/mm/dd'
            });
        });

    </script>

</body>

</html>
