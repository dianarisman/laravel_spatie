<div class="box-body">
  @php
    $index = 0;
  @endphp
  @foreach ($items as $item)
    
          @php
            $stringFormat =  strtolower(str_replace(' ', '', $item));
          @endphp
         
        <div class="col-md-2">
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" value="{{isset($oldVals) ? $oldVals[$index] : ''}}" name="<?=$stringFormat?>" class="form-control pull-right" id="<?=$stringFormat?>" placeholder="{{$item}}" required>
            </div>
        </div>
 
  @php
    $index++;
  @endphp
  @endforeach
</div>