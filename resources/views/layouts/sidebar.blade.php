<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="/" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
           
            <li>
                <a href="{{url('karyawan')}}"><i class="fa fa-users fa-fw"></i> Karyawan</a>
            </li>
            <li>
                <a href="{{url('jabatan')}}"><i class="fa fa-edit fa-fw"></i> Jabatan</a>
            </li>
            <li>
                <a href="{{url('laporanabsensi')}}"><i class="fa fa-calendar fa-fw"></i> Loporan Absensi</a>
            </li>
            <li>
                <a href="{{url('laporanlembur')}}"><i class="fa fa-calendar-check-o fa-fw"></i> Laporan Lembur</a>
            </li>
            <li>
                <a href="{{url('lokasi')}}"><i class="fa fa-map fa-fw"></i> Lokasi Kantor</a>
            </li>
           
        </ul>
    </div>
</div>
