@extends ('layouts.main')

@section('layout')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tambah Lokasi</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('lokasi.store') }}">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" />
            </div>
            <div class="form-group">
                <label>Latitude</label>
                <input type="text" class="form-control" name="lat" />
            </div>
            <div class="form-group">
                <label>Longitude</label>
                <input type="text" class="form-control" name="long" />
            </div>
            <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
    </div>
</div>
@endsection
